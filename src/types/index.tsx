export type GithubIssueItem = {
  url: string;
  id: string;
  views: number;
  title: string;
  // labels: {
  //     name: string;
  //     color: string;
  // }[];
  state: string;
  comments: number;
  name: string;
  path: string;
  closed_at?: string;
};
