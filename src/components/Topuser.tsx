import React, { useState, useEffect } from 'react';
import { Avatar, Popover, message, Button } from 'antd';
import { history } from 'umi';
import { UserOutlined } from '@ant-design/icons';
export default function Topuser() {
  let [name, setName]: any = useState({});
  useEffect(() => {
    setName(localStorage.userInfo && JSON.parse(localStorage.userInfo));
  }, []);
  const content = (
    <ul className="ul">
      <li
        onClick={() => {
          localStorage.clear();
          message.info('退出登录');
          setTimeout(() => {
            history.push('/login');
          }, 2000);
        }}
      >
        退出登录
      </li>
    </ul>
  );
  return (
    <div>
      <Popover placement="bottom" content={content}>
        <Avatar
          style={{ marginBottom: 13, marginRight: 5 }}
          icon={<UserOutlined />}
        />
        你好
        {name && (
          <span
            style={{ fontSize: 16, marginLeft: 10, display: 'inline-block' }}
          >
            {name.name}
          </span>
        )}
      </Popover>
    </div>
  );
}
