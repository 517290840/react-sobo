import { request } from 'umi';
export interface LoginTop {
  name: string;
  password: string;
}
export interface Pamser {
  page: number;
  pageSize: number;
}
export interface knowledgeDelete {
  id: string;
}
export interface AllPamser {
  page: number;
  pageSize: number;
}
export const login = (params: LoginTop) => {
  let res = request('/api/auth/login', {
    method: 'post',
    data: params,
    skipErrorHandler: true,
  });
  return res;
};
export const searchList = (params: Pamser) => {
  return request('/api/search', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
export const searchListData = () => {
  return request('/api/search', {
    method: 'get',

    skipErrorHandler: true,
  });
};
export const filData = (params: Pamser) => {
  return request('/api/file', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};

export const ViewList = (params: Pamser) => {
  return request('/api/view', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
export const commentData = (params: Pamser) => {
  return request('/api/comment', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
export const categoryData = (params: Pamser) => {
  return request('/api/category', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
export const articleData = (params: Pamser) => {
  return request('/api/article', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
export const mailData = (params: Pamser) => {
  return request('/api/smtp', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
export const userData = (params: Pamser) => {
  return request('/api/user', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
export const getArticle = (params: Pamser) => {
  return request('/api/article', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
//知识小册
export const Knowledge = (params: AllPamser) => {
  return request('/api/Knowledge', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
export const page = (params: AllPamser) => {
  return request('/api/page', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
export let fileDelete = (params: fileDelete) => {
  let res = request(`/api/file/` + params, {
    method: 'delete',
    skipErrorHandler: true,
  });
  return res;
};
export interface fileDelete {
  id: string;
}
export interface search {
  page: number;
  pageSize: number;
}
export let search = async (params: search) => {
  let res = await request('/api/search', {
    method: 'get',
    params: params,
    skipErrorHandler: true,
  });
  return res;
};
export const SearchDelete = async (params: mailDelete) => {
  let res = await request('/api/search/' + params, {
    method: 'delete',
    skipErrorHandler: true,
  });
  return res;
};
export interface mailDelete {
  id: 'string';
}
export interface sign {
  name: 'string';
  password: 'string';
  confirm: 'string';
}
export let sign = (params: sign) => {
  let res = request(`/api/user/register`, {
    method: 'post',
    data: params,
    skipErrorHandler: true,
  });
  return res;
};
