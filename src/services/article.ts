import { request } from 'umi';
export interface categoryList {
  articleCount: number;
  createAt: string;
  id: string;
  label: string;
  updateAt: string;
  value: string;
}
export const category = (params: categoryList) => {
  let res = request('/api/category', {
    method: 'get',
    data: params,
    skipErrorHandler: true,
  });
  return res;
};
