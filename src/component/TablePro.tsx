import React, { useRef } from 'react';
import { PlusOutlined, EllipsisOutlined } from '@ant-design/icons';
import { Button, Tag, Space, Menu, Dropdown, Badge } from 'antd';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable, { TableDropdown } from '@ant-design/pro-table';
import request from 'umi-request';
import { history } from 'umi';

//新建
const newBtn: any = () => {
  console.log('新建');
  history.push('/article/tagsarticle');
};
type GithubIssueItem = {
  url: string;
  id: string;
  views: number;
  title: string;
  // labels: {
  //     name: string;
  //     color: string;
  // }[];
  state: string;
  comments: number;
  name: string;
  path: string;
  closed_at?: string;
};

export default (props: any) => {
  let {
    columns,
    scroll,
    request,
    headerTitle,
    tableAlertRender,
    tableAlertOptionRender,
  } = props;
  const actionRef = useRef<ActionType>();
  return (
    <ProTable<GithubIssueItem>
      columns={columns} //表格
      actionRef={actionRef}
      request={request} //添加获取事件函数 ，返回值{data,total}
      editable={{
        type: 'multiple',
      }}
      columnsState={{
        persistenceKey: 'pro-table-singe-demos',
        persistenceType: 'localStorage',
      }}
      rowKey="id" //渲染的key值
      rowSelection={{}} //添加单选框属性
      scroll={scroll} //滑动的宽度
      search={{
        // labelWidth: 300,//搜索框的宽度
        // span:10,//搜索框排版
        // defaultCollapsed:false,
        collapsed: false, //
        collapseRender: false, //收起按钮
      }}
      pagination={{
        pageSize: 10,
      }} //分页
      tableAlertRender={tableAlertRender ? tableAlertRender : false} //表格左侧
      tableAlertOptionRender={
        tableAlertOptionRender ? tableAlertOptionRender : false
      } //自定义批量操作工具栏左侧信息区域, false 时不显示
      dateFormatter="string"
      headerTitle={headerTitle ? headerTitle : ''} //表格标题
      toolBarRender={() => [
        <Button
          key="button"
          icon={<PlusOutlined />}
          type="primary"
          onClick={newBtn}
        >
          新建
        </Button>,
        // <Dropdown key="menu" overlay={menu}>
        //     <Button>
        //         <EllipsisOutlined />
        //     </Button>
        // </Dropdown>,
      ]}
    />
  );
};
