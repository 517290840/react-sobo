import React from 'react';
import { Empty } from 'antd';
export default function No() {
  return (
    <div>
      <Empty />
    </div>
  );
}
