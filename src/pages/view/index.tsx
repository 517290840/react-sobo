// import styles from './index.less';
import React, { FC, useEffect, useState } from 'react';
import { Table, Card, Input, Pagination } from 'antd';
import './index.less';
import axios from 'axios';
import { getArticle } from '@/services/user';
import { IndexModelType, ConnectProps, Loading, connect, request } from 'umi';
interface PageProps extends ConnectProps {
  index: IndexModelType;
  loading: boolean;
}
const All: FC<PageProps> = ({ index, dispatch }) => {
  console.log(index, 'axios');
  function onShowSizeChange(current: any, pageSize: any) {
    console.log(current, pageSize);

    dispatch!({
      type: 'index/fintnums',
      payload: { page: current + 1, pageSize: pageSize },
    });
  }
  function gitSize(page: any, pageSize: any) {
    dispatch!({
      type: 'index/fintnums',
      payload: { page: page, pageSize: 10 },
    });
  }
  useEffect(() => {
    dispatch!({
      type: 'index/fintnums',
      payload: { page: 1, pageSize: 10 },
    });
  }, [index.total]);
  let dataList: Array<any> = useState(index);

  const columns: Array<any> = [
    {
      title: 'URL',
      width: 100,
      dataIndex: 'url',
      key: 'name',
    },
    {
      title: 'IP',
      dataIndex: 'ip',
      key: '1',
      width: 150,
    },
    {
      title: '浏览器',
      dataIndex: 'browser',
      key: '2',
      width: 150,
    },
    {
      title: '内核',
      dataIndex: 'engine',
      key: '2',
      width: 50,
    },
    {
      title: '操作系统',
      dataIndex: 'os',
      key: '2',
      width: 50,
    },
    {
      title: '设备',
      dataIndex: '',
      key: '2',
      width: 50,
    },
    {
      title: '地址',
      dataIndex: 'address',
      key: '2',
      width: 50,
    },
    {
      title: '访问量',
      dataIndex: 'count',
      key: '2',
      width: 50,
    },
    {
      title: '访问时间  ',
      dataIndex: 'createAt',
      key: '2',
      width: 50,
    },
    {
      title: 'Action',

      dataIndex: 'id',
      fixed: 'right',
      width: 50,
      render: (item: string) => (
        <a
          onClick={() => {
            request(`/api/view/${item}`, {
              method: 'delete',
              skipErrorHandler: true,
            });
            dispatch!({
              type: 'index/sreatchLists',
              payload: { page: 1, pageSize: 12 },
            });
          }}
        >
          删除
        </a>
      ),
    },
  ];
  //  axios.get("/api/comment").then(res=>{
  //    console.log(res.data)
  //  })

  const rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        'selectedRows: ',
        selectedRows,
      );
    },
    getCheckboxProps: (record: { name: string }) => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
  };
  return (
    <div>
      <div className="example-input">
        <span>
          {' '}
          IP:
          <Input placeholder="default size" />
        </span>
        <span>
          {' '}
          UA:
          <Input placeholder="default size" />
        </span>
        <span>
          {' '}
          URL:
          <Input placeholder="default size" />
        </span>

        <span>
          {' '}
          地址:
          <Input placeholder="default size" />
        </span>
        <br />
        <span>
          {' '}
          浏览器:
          <Input placeholder="default size" />
        </span>
        <span>
          {' '}
          内核:
          <Input placeholder="default size" />
        </span>

        <span>
          {' '}
          OS:
          <Input placeholder="default size" />
        </span>
        <span>
          {' '}
          设备:
          <Input placeholder="default size" />
        </span>
      </div>
      ,
      <div className="conten">
        <Table
          pagination={false}
          columns={columns}
          dataSource={index.ViewDta}
          scroll={{ x: 1500, y: 300 }}
        />
        <div>
          <Pagination
            showSizeChanger
            onShowSizeChange={onShowSizeChange}
            defaultCurrent={1}
            onChange={gitSize}
            total={index.total}
          />
        </div>
      </div>
    </div>
  );
};
export default connect((state: any) => {
  return state;
})(All);
