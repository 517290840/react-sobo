// import styles from './index.less';
import React, { useState, useEffect, FC } from 'react';
import {
  Table,
  Card,
  Pagination,
  Drawer,
  Button,
  Tabs,
  Tag,
  Input,
} from 'antd';
import { request } from 'umi';
import { YoutubeFilled } from '@ant-design/icons';
import { IndexModelType, ConnectProps, Loading, connect } from 'umi';
const { TabPane } = Tabs;
const { TextArea } = Input;
import axios from 'axios';
import './index.less';

interface PageProps extends ConnectProps {
  index: IndexModelType;
  loading: boolean;
}

const string: FC<PageProps> = ({ index, dispatch }) => {
  const [listData, userdata]: any = useState([]);
  function onShowSizeChange(current: any, pageSize: any) {
    console.log(current, pageSize);
    dispatch!({
      type: 'index/fintData',
      payload: { page: current + 1, pageSize: pageSize },
    });
  }
  function gitSize(page: any, pageSize: any) {
    dispatch!({
      type: 'index/fintData',
      payload: { page: page, pageSize: 10 },
    });
  }
  useEffect(() => {
    axios.post('/api/setting/get').then((res) => {
      userdata(res.data.data);
      console.log(listData);
    });
  }, [listData.adminSystemUrl]);

  const { Meta } = Card;
  const [visible, setVisible] = useState(false);
  let showDrawer = () => {
    setVisible(true);
    console.log('243576');
    dispatch!({
      type: 'index/fintData',
      payload: { page: 1, pageSize: 10 },
    });
  };

  let onClose = () => {
    setVisible(false);
  };
  return (
    <div>
      <div className="top">asdfghj</div>
      <div>
        <Tabs tabPosition="left">
          <TabPane tab="系统设置" key="1">
            系统设置
            <Input value={listData.adminSystemUrl} />
            后台设置
            <Input value={listData.systemUrl} />
            系统标题
            <Input value={listData.systemTitle} />
            Logo
            <Input
              addonAfter={<YoutubeFilled onClick={showDrawer} />}
              value={listData.systemLogo}
            />
            Favicon
            <Input
              addonAfter={<YoutubeFilled onClick={showDrawer} />}
              value={listData.systemFavicon}
            />
            页脚信息
            <TextArea rows={4} value={listData.systemFooterInfo} />
            {}
          </TabPane>
          <TabPane tab="国际化设置" key="2"></TabPane>
          <TabPane tab="SEO 设置  " key="3">
            百度统计
            <Input value={listData.systemUrl} />
            谷歌分析
            <Input value={listData.systemTitle} />
          </TabPane>
          <TabPane tab="数据  " key="4">
            百度统计
            <Input value={listData.systemUrl} />
            谷歌分析
            <Input value={listData.systemTitle} />
          </TabPane>
          <TabPane tab="OSS 设置" key="5">
            Content of Tab 2
          </TabPane>
          <TabPane tab="SMTP  服务" key="6">
            SMTP地址
            <Input value={listData.adminSystemUrl} />
            SMTP端口(强制使用SSL链接)
            <Input value={listData.systemUrl} />
            SMTP用户
            <Input value={listData.systemTitle} />
            SMTP密码
            <Input addonAfter={<YoutubeFilled onClick={showDrawer} />} />
            发件人
            <Input addonAfter={<YoutubeFilled onClick={showDrawer} />} />
          </TabPane>
        </Tabs>
      </div>
      <Drawer
        title="你看我是谁"
        placement="right"
        onClose={onClose}
        visible={visible}
        width={720}
      >
        {index.filTdata && index.filTdata.length > 0
          ? index.filTdata.map((item: any, index: any) => {
              return (
                <Card
                  hoverable
                  key={index}
                  style={{ width: 240, float: 'left' }}
                  cover={<img alt="example" src={item.url} />}
                  onClick={() => {
                    setVisible(true);
                  }}
                >
                  <Meta title={item.url} description={item.createAt} />
                </Card>
              );
            })
          : null}
        <Pagination
          showSizeChanger
          onShowSizeChange={onShowSizeChange}
          defaultCurrent={1}
          onChange={gitSize}
          total={index.total}
        />
      </Drawer>
    </div>
  );
};
export default connect((state: any) => {
  return state;
})(string);
