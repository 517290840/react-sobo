import React, { FC, useEffect, useState } from 'react';
import './style.less';
import axios from 'axios';
import { Table, Card, Input, Space } from 'antd';
import { IndexModelType, ConnectProps, Loading, connect, request } from 'umi';

interface PageProps extends ConnectProps {
  index: IndexModelType;
  loading: boolean;
}
const comment: FC<PageProps> = ({ index, dispatch }) => {
  console.log(index);

  useEffect(() => {
    dispatch!({
      type: 'index/commentent',
      payload: { page: 1, pageSize: 12 },
    });
  }, [index.comment]);

  console.log(index);

  const columns: Array<any> = [
    {
      title: '状态',
      width: 100,
      dataIndex: 'pass',
      key: 'pass',
      fixed: 'left',
      render: (item: string) =>
        item === 'draft' ? (
          <li>
            <span></span>
            <span>未通过</span>
          </li>
        ) : (
          <li>
            <span></span>
            <span>通过</span>
          </li>
        ),
    },
    {
      title: '称呼',
      width: 100,
      dataIndex: 'name',
      key: 'name',
      fixed: 'left',
    },
    {
      title: '联系方式',
      dataIndex: 'email',
      key: '1',
      width: 150,
    },
    {
      title: '原始内容',
      dataIndex: 'name',
      key: '2',
      width: 150,
    },
    {
      title: 'HTML内容',
      dataIndex: 'name',
      key: '2',
      width: 150,
    },
    {
      title: '管理文章',
      dataIndex: 'name',
      key: '2',
      width: 150,
    },
    {
      title: '创建时间',
      dataIndex: 'createAt',
      key: '2',
      width: 150,
    },
    {
      title: '父级评论',
      dataIndex: 'replyUserName',
      key: '2',
      width: 150,
    },
    {
      title: '操作',

      dataIndex: 'id',
      fixed: 'right',
      width: 200,
      render: (item: string) => (
        <Space size="middle">
          <a>通过</a>
          <a>拒绝</a>
          <a>回复</a>
          <a
            onClick={() => {
              request(`/api/comment/${item}`, {
                method: 'delete',

                skipErrorHandler: true,
              });
              dispatch!({
                type: 'index/commentent',
                payload: { page: 1, pageSize: 12 },
              });
            }}
          >
            删除
          </a>
        </Space>
      ),
    },
  ];

  const rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        'selectedRows: ',
        selectedRows,
      );
    },
    getCheckboxProps: (record: { name: string }) => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
  };
  return (
    <div>
      <div className="TheHead">
        <div className="breadcrumb">
          <span>
            <a href="/" className="workbench">
              工作台 /
            </a>
          </span>
          <span>
            <a href="/page" className="page">
              评论管理
            </a>
          </span>
        </div>
      </div>

      <div className="legacy">
        <div className="operation">
          <div className="example-input">
            <span>
              {' '}
              类型:
              <Input placeholder="default size" />
            </span>
            <span>
              {' '}
              关键词:
              <Input placeholder="default size" />
            </span>
            <span>
              {' '}
              搜索量:
              <Input placeholder="default size" />
            </span>
          </div>
        </div>
        <div className="form">
          <Table
            rowSelection={rowSelection}
            columns={columns}
            dataSource={index.comment}
            scroll={{ x: 600, y: 400 }}
          />
        </div>
      </div>
    </div>
  );
};

export default connect((state: any) => {
  return state;
})(comment);
