import react, { Component, useState, useEffect } from 'react';
import styles from './index.less';
import { Select, Card, Avatar, Popconfirm, message } from 'antd';
import {
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined,
} from '@ant-design/icons';
const { Option } = Select;
const { Meta } = Card;

import { Knowledge, knowledgeDelete } from '@/services/user';

function handleChange(value: any) {
  console.log(`selected ${value}`);
}
const confirm = async (id: any) => {
  console.log(id);
  await knowledgeDelete(id);
  message.success('Click on Yes');
};

function cancel(e: any) {
  console.log(e);
  message.error('Click on No');
}
//类型
interface pages {
  summary: 'string';
}

function Knowledges() {
  let [list, setList]: any = useState([]);
  let [page, setpage] = useState(1);
  let [pageSize, setPageSize] = useState(12);

  let getList = async () => {
    let res = await Knowledge({ page: page, pageSize: pageSize });
    // console.log(res);

    setList(res.data[0]);
    console.log(list);
  };

  useEffect(() => {
    getList();
  }, []);

  return (
    <div>
      <div className={styles.TheHead}>
        <div className={styles.breadcrumb}>
          <span>
            <a href="/" className={styles.workbench}>
              工作台 /
            </a>
          </span>
          <span>
            <a href="/knowledge" className={styles.page}>
              知识小册
            </a>
          </span>
        </div>
      </div>

      <div className={styles.legacy}>
        <div className={styles.operation}>
          <div className={styles.row}>
            <span>
              名称：
              <input type="text" placeholder="请输入页面名称" />
            </span>
            <span>
              状态：
              <Select
                defaultValue="lucy"
                style={{ width: 180 }}
                onChange={handleChange}
              >
                <Option value="jack">已发布</Option>
                <Option value="lucy">草稿</Option>
              </Select>
            </span>
          </div>

          <div className={styles.buttons}>
            <button className={styles.search}>搜索</button>
            <button className={styles.reset}>重置</button>
          </div>
        </div>
        <div className={styles.form}>
          {list.map((item: any, index: any) => {
            return (
              <div>
                <Card
                  style={{ width: 300, height: 222 }}
                  cover={
                    <img
                      style={{ width: 300, height: 222 }}
                      alt="example"
                      src={item.cover}
                    />
                  }
                  actions={[
                    <Popconfirm
                      title="Are you sure delete this task?"
                      onConfirm={() => confirm(item.id)}
                      onCancel={cancel}
                      okText="确定"
                      cancelText="取消"
                    >
                      <SettingOutlined key="setting" />,
                      <EditOutlined key="edit" />,
                      <EllipsisOutlined key="ellipsis" />,
                    </Popconfirm>,
                  ]}
                >
                  <Meta
                    avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
                    title={item.title}
                    description={item.summary}
                  />
                </Card>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default Knowledges;
