import styles from './index.less';
import { Empty } from 'antd';
export default function poster() {
  return (
    <div>
      <div className={styles.TheHead}>
        <div className={styles.breadcrumb}>
          <span>
            <a href="/" className={styles.workbench}>
              工作台 /
            </a>
          </span>
          <span>
            <a href="/poster" className={styles.page}>
              海报管理
            </a>
          </span>
        </div>
      </div>

      <div className={styles.legacy}>
        <div className={styles.operation}>
          <div className={styles.row}>
            <span>
              名称：
              <input type="text" placeholder="请输入页面名称" />
            </span>
          </div>
          <div className={styles.buttons}>
            <button className={styles.search}>搜索</button>
            <button className={styles.reset}>重置</button>
          </div>
        </div>
        <div className={styles.form}>
          <Empty
            className={styles.stateless}
            image={Empty.PRESENTED_IMAGE_SIMPLE}
          />
        </div>
        <div>
          {/* render={(item) =>
                  item === 'draft' ? (
                    <li>
                      <span></span>
                      <span>草稿</span>
                    </li>
                  ) : (
                    <li>
                      <span></span>
                      <span>已发布</span>
                    </li>
                  )
                } */}
        </div>
      </div>
    </div>
  );
}
