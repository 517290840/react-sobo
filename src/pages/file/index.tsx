import React, { useState, useEffect, useRef } from 'react';
import { filData } from '@/services/user';
import './index.less';
import { Card, Col, Row, Pagination, Drawer, Button, Input, Empty } from 'antd';
import No from '@/component/No';
const { TextArea } = Input;
import moment from 'moment';
const { Meta } = Card;
import ProForm, {
  ProFormSwitch,
  ProFormText,
  ProFormRadio,
  ProFormCheckbox,
  ProFormRate,
  ProFormSelect,
  ProFormDigit,
  ProFormSlider,
  ProFormGroup,
} from '@ant-design/pro-form';

interface pages {
  createAt: 'string';
  filename: 'string';
  id: 'string';
  originalname: 'string';
  size: 'string';
  type: 'string';
  url: 'string';
}

let Document = () => {
  let [list, setList]: any = useState([]);
  let [page, setPage]: any = useState(1);
  let [pageSize, setpageSize]: any = useState(12);
  let [total, setTotal]: any = useState(0);
  let [misk, setMisk]: any = useState({});
  let [originalname, setOriginalname]: any = useState('');
  let [type, setType]: any = useState('');

  let getList = async () => {
    let res = await filData({ page, pageSize, originalname, type });
    setList(res.data[0]);
    setTotal(res.data[1]);
  };
  let clickPage = (page: number) => {
    setPage(page);
    getList();
  };
  const [visible, setVisible] = useState(false);
  const showDrawer = (item: any) => {
    setVisible(true);
    setMisk(item);
  };
  const onClose = () => {
    setVisible(false);
  };
  const misks = () => {
    setVisible(false);
  };
  const searchHandle = async (value: any) => {
    let { originalname, type } = value;
    setType(type ? type.trim() : '');
    setOriginalname(originalname ? originalname.trim() : '');
    // getList()
  };

  const DeltetHandle = async (id: any) => {
    console.log(id);

    let res = await fileDelete(id);
    console.log(res);
  };

  useEffect(() => {
    getList();
  }, [page, type, originalname]);

  return (
    <div className="document">
      <div className="document_header">
        &nbsp;&nbsp; 系统检测到<span id="io">阿里云OSS配置</span>未完善,{' '}
        <span
          style={{ cursor: 'pointer' }}
          onClick={() => {
            location.replace('/page/system');
          }}
        >
          点我立即完善
        </span>
      </div>
      <ProForm
        className="document_top"
        onFinish={async (value: any) => {
          searchHandle(value);
        }}
      >
        <ProFormGroup>
          文件名称:
          <ProFormText width="md" name="originalname" />
          文件类型:
          <ProFormText width="md" name="type" />
        </ProFormGroup>
      </ProForm>
      <div className="document_bottom">
        {list.length > 0 ? (
          <div>
            <div className="site-card-wrapper">
              <Row gutter={24}>
                {list && list.length > 0 ? (
                  list.map((item: any, index: any) => {
                    return (
                      <Col span={6} key={item.id}>
                        <Card
                          hoverable
                          bordered={true}
                          onClick={() => showDrawer(item)}
                          className="document_col"
                          cover={<img alt="example" src={item.url} />}
                        >
                          <Meta title={item.originalname} />
                          <Meta
                            title={moment(item.createAt).format(
                              'YYY-MM-DD HH:mm:ss',
                            )}
                          />
                        </Card>
                      </Col>
                    );
                  })
                ) : (
                  <No />
                )}
              </Row>

              <Drawer
                placement="right"
                onClose={onClose}
                visible={visible}
                width={580}
                style={{ overflowY: 'hidden' }}
                closable={false}
                keyboard={true}
              >
                <h2>文件信息</h2>
                <p>
                  <img
                    src={misk.url}
                    alt=""
                    style={{ width: 535, height: 330, margin: 2 }}
                  />
                </p>
                <p style={{ marginLeft: 4 }}>文件名称:{misk.filename} </p>
                <p style={{ marginLeft: 4 }}>储存路径:{misk.url}</p>
                <p style={{ marginLeft: 4 }}>
                  文件类型:{misk.type}
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;文件大小:
                  {misk.size / 1024 + 'KB'}
                </p>
                <p style={{ marginLeft: 4 }}>
                  {' '}
                  访问链接:
                  <TextArea value={misk.url} />
                </p>
                <Button onClick={misks} style={{ marginLeft: 4 }}>
                  关闭
                </Button>{' '}
                <Button danger onClick={() => DeltetHandle(misk.id)}>
                  删除
                </Button>
              </Drawer>
            </div>

            <div className="bottom_page">
              共:{total}条<Pagination total={total} onChange={clickPage} />
            </div>
          </div>
        ) : (
          <Empty />
        )}
      </div>
    </div>
  );
};

export default Document;
