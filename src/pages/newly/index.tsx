import MonacoEditor from 'react-monaco-editor';
import MarkdownIt from 'markdown-it';
import hljs from 'highlight.js';
import { useState, useEffect, useRef } from 'react';
export default function Types() {
  const content = useRef<any>(null);
  const [html, setHtml] = useState('');
  useEffect(() => {
    content.current = new MarkdownIt({
      html: false,
      xhtmlOut: false,
      breaks: false,
      langPrefix: 'language-',
      linkify: true,
      typographer: false,
      quotes: '“”‘’',
      highlight: (str: string, lang: string) => {
        // 此处判断是否有添加代码语言
        if (lang && hljs.getLanguage(lang)) {
          try {
            // 得到经过highlight.js之后的html代码
            const preCode = hljs.highlight(lang, str, true).value;
            // 通过换行进行分割
            const lines = preCode.split(/\n/).slice(0, -1);
            // 添加自定义行号
            let html = lines
              .map((item, index) => {
                return (
                  '<li><span class="line-num data-line="' +
                  (index + 1) +
                  '"></span>' +
                  item +
                  '</li>'
                );
              })
              .join('');
            html = '<ol>' + html + '</ol>';
            // 添加代码语言//
            if (lines.length) {
              //
              html += '<b class="name">' + lang + '</b>'; //
            }
            return '<pre class="hljs"><code>' + html + '</code></pre>';
          } catch (_) {}
        }
        // 为添加代码语言，此处与上面同理
        const preCode: any = content.current.utils.escapeHtml(str);
        const lines = preCode.split(/\n/).slice(0, -1);
        let html = lines
          .map((item: any, index: number) => {
            return (
              '<li><span class="line-num data-line="' +
              (index + 1) +
              '"></span>' +
              item +
              '</li>'
            );
          })
          .join('');
        html = '<ol>' + html + '</ol>';
        return '<pre class="hljs"><code>' + html + '</code></pre>';
      },
    });
  }, []);
  const onChange = (newValue: any, e: any) => {
    // newValue左侧编辑框的内容
    // console.log('onChange', newValue, e);
    setHtml(content.current.render(newValue));
    //
    // console.log(html, '******');
  };
  return (
    <div>
      <h1>types</h1>
      <MonacoEditor
        width="800"
        height="600"
        language="javascript"
        onChange={onChange}
      />
      <div
        className="markdown"
        dangerouslySetInnerHTML={{ __html: html }}
      ></div>
    </div>
  );
}
