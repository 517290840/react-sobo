import React, { FC, useEffect, useState } from 'react';
import './index.less';
import axios from 'axios';
import { Table, Card, Input, Space } from 'antd';
import { IndexModelType, ConnectProps, Loading, connect, request } from 'umi';

interface PageProps extends ConnectProps {
  index: IndexModelType;
  loading: boolean;
}

const user: FC<PageProps> = ({ index, dispatch }) => {
  console.log(index);

  useEffect(() => {
    dispatch!({
      type: 'index/users',
      payload: { page: 1, pageSize: 12 },
    });
  }, []);
  console.log(index);

  const columns: Array<any> = [
    {
      title: '账户',
      width: 100,
      dataIndex: 'name',
      key: 'name',
      // fixed: 'left',
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      key: '1',
      width: 100,
    },
    {
      title: '角色',
      dataIndex: 'role',
      key: '2',
      width: 150,
      render: (item: string) =>
        item === 'visitor' ? (
          <li>
            <span></span>
            <span>访客</span>
          </li>
        ) : (
          <li>
            <span></span>
            <span>管理员</span>
          </li>
        ),
    },
    {
      title: '注册日期',
      dataIndex: 'createAt',
      key: '2',
      width: 150,
    },
    {
      title: '操作',

      dataIndex: 'id',
      // fixed: 'right',
      width: 200,
      render: (item: string) => (
        <Space size="middle">
          <a>启用授权</a>
          <a>解除</a>
        </Space>
      ),
    },
  ];
  const rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        'selectedRows: ',
        selectedRows,
      );
    },
    getCheckboxProps: (record: { name: string }) => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
  };

  return (
    <div>
      <div className="TheHead">
        <div className="breadcrumb">
          <span>
            <a href="/" className="workbench">
              工作台 /
            </a>
          </span>
          <span>
            <a href="/page" className="page">
              用户管理
            </a>
          </span>
        </div>
      </div>

      <div className="legacy">
        <div className="operation">
          <div className="example-input">
            <span>
              {' '}
              类型:
              <Input placeholder="default size" />
            </span>
            <span>
              {' '}
              关键词:
              <Input placeholder="default size" />
            </span>
            <span>
              {' '}
              搜索量:
              <Input placeholder="default size" />
            </span>
          </div>
        </div>
        <div className="form">
          <Table
            rowSelection={rowSelection}
            columns={columns}
            dataSource={index.users}
            scroll={{ x: 600, y: 400 }}
          />
        </div>
      </div>
    </div>
  );
};

export default connect((state: any) => {
  return state;
})(user);
