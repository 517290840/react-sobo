import react, { useState, useEffect } from 'react';
import { Input, Cascader, Table, Breadcrumb } from 'antd';
import allarticle from './allarticle.less';
import { articleData, categoryData } from '@/services/user';
function tagsarticle() {
  function btn_delete(item: any) {
    setlist(list.filter((ite: any) => item.id != ite.id));
  }
  function onChange_fl(values: any) {
    setlist(list.filter((item: any) => item.category != null));
  }
  function onChange_zt(values: any) {
    setlist(list.filter((item: any) => item.status === values[0]));
  }
  function onChange(e: any) {
    setlist(list.filter((ite: any) => ite.title.includes(e.target.value)));
  }
  let [list, setlist]: any = useState([]);
  let [lists, setlists]: any = useState([]);
  let [page, setpage] = useState(1);
  let [pageSize, setPageSize] = useState(12);
  let options = [
    {
      value: 'publish',
      label: '已发布',
    },
    {
      value: 'draft',
      label: '草稿',
    },
  ];
  let columns: any = [
    {
      title: '标题',
      width: 100,
      dataIndex: 'title',
      key: 'name',
      fixed: 'left',
    },
    {
      title: '分类',
      dataIndex: 'status',
      render: (item: any) =>
        item === 'draft' ? (
          <li>
            <span>草稿</span>
          </li>
        ) : (
          <li>
            <span>已发布</span>
          </li>
        ),
      key: '2',
    },
    {
      title: '类型',
      dataIndex: 'category',
      render: (item: any) => (item != null ? <span>{item.value}</span> : null),
      key: '5',
    },
    { title: '阅读量', dataIndex: 'views', key: '6' },
    { title: '喜欢数', dataIndex: 'likes', key: '7' },
    { title: '发布时间', dataIndex: 'createAt', key: '8' },
    {
      title: '操作',
      key: '9',
      fixed: 'right',
      width: 230,
      render: (item: any) => (
        <div className={allarticle.table_a}>
          <a>编辑</a>
          <a>操作</a>
          <a>访问</a>
          <a
            onClick={() => {
              btn_delete(item);
            }}
          >
            删除
          </a>
        </div>
      ),
    },
  ];
  let artlist = async () => {
    let res = await articleData({ page: page, pageSize: pageSize });
    setlist(res.data[0]);
  };
  let catlist = async () => {
    let res = await categoryData({ page: page, pageSize: pageSize });
    setlists(res.data);
    // console.log(lists);
  };
  useEffect(() => {
    artlist();
    catlist();
  }, []);
  return (
    <div className={allarticle.allarticle}>
      {console.log(list)}
      <div className={allarticle.header}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/">工作台 </a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/article">文章管理</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/article/allarticle">所有文章</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className={allarticle.main}>
        <div className={allarticle.main_top}>
          <div>
            标题：
            <Input
              style={{ width: 120 }}
              placeholder="请输入文章标题"
              onChange={onChange}
            />
          </div>
          <div>
            状态：
            <Cascader
              options={options}
              placeholder=""
              onChange={(value) => {
                onChange_zt(value);
              }}
            />
          </div>
          <div>
            <Cascader
              options={lists}
              onChange={(value) => {
                onChange_fl(value);
              }}
              placeholder=""
            />
          </div>
          <div></div>
          <div></div>
        </div>
        <div className={allarticle.main_bottom}>
          <Table columns={columns} dataSource={list} scroll={{ x: 1300 }} />
        </div>
      </div>
    </div>
  );
}
export default tagsarticle;
