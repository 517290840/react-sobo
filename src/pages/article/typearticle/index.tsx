import React, { Component } from 'react';
import allarticle from '../allarticle/allarticle.less';
import { Input, Button } from 'antd';
import axios from 'axios';

class typearticle extends Component {
  state = {
    typeList: [],
  };
  componentDidMount() {
    axios.get('/api/category').then((res) => {
      this.setState({
        typeList: res.data.data,
      });
    });
  }
  render() {
    console.log(this.state.typeList);
    return (
      <div className={allarticle.allarticle}>
        <div className={allarticle.header}>
          <div>
            <span>
              <a href="/">工作台 /</a>
            </span>
            <span>
              <a href="/article">文章管理/</a>
            </span>
            <span>
              <a href="/article/typearticle">分类管理</a>
            </span>
          </div>
        </div>
        <div className={allarticle.main_type}>
          <div className={allarticle.main_left}>
            <div className={allarticle.main_left_top}>添加分类</div>
            <div className={allarticle.main_left_buttom}>
              <div className={allarticle.main_left_buttom_div}>
                <Input placeholder="" />
              </div>
              <div className={allarticle.main_left_buttom_div}>
                <Input placeholder="" />
              </div>
              <div className={allarticle.main_left_buttom_div}>
                <Button type="primary">确认</Button>
              </div>
            </div>
          </div>
          <div className={allarticle.main_right}>
            <div className={allarticle.main_right_top}>所有分类</div>
            <div className={allarticle.main_right_buttom}>
              {this.state.typeList.map((item: any) => {
                return (
                  <div className={allarticle.main_right_top_div}>
                    {item.value}
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default typearticle;
