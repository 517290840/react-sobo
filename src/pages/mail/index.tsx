import React, { FC, useEffect, useState } from 'react';
import './index.less';
import axios from 'axios';
import { Table, Card, Input, Space } from 'antd';
import { IndexModelType, ConnectProps, Loading, connect, request } from 'umi';

interface PageProps extends ConnectProps {
  index: IndexModelType;
  loading: boolean;
}

const mail: FC<PageProps> = ({ index, dispatch }) => {
  console.log(index);

  useEffect(() => {
    dispatch!({
      type: 'index/mails',
      payload: { page: 1, pageSize: 12 },
    });
  }, []);
  console.log(index);

  const columns: Array<any> = [
    {
      title: '发件人',
      width: 400,
      dataIndex: 'from',
      key: 'from',
      // fixed: 'left',
    },
    {
      title: '收件人',
      dataIndex: 'to',
      key: '1',
      width: 400,
    },
    {
      title: '主题',
      dataIndex: 'subject',
      key: '2',
      width: 150,
    },
    {
      title: '发送时间',
      dataIndex: 'createAt',
      key: '2',
      width: 150,
    },
    {
      title: '操作',

      dataIndex: 'id',
      // fixed: 'right',
      width: 200,
      render: (item: string) => (
        <Space size="middle">
          <a
            onClick={() => {
              request(`/api/mail/${item}`, {
                method: 'delete',

                skipErrorHandler: true,
              });
              dispatch!({
                type: 'index/mails',
                payload: { page: 1, pageSize: 12 },
              });
            }}
          >
            删除
          </a>
        </Space>
      ),
    },
  ];
  const rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        'selectedRows: ',
        selectedRows,
      );
    },
    getCheckboxProps: (record: { name: string }) => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
  };

  return (
    <div>
      <div className="TheHead">
        <div className="breadcrumb">
          <span>
            <a href="/" className="workbench">
              工作台 /
            </a>
          </span>
          <span>
            <a href="/page" className="page">
              邮件管理
            </a>
          </span>
        </div>
      </div>

      <div className="legacy">
        <div className="operation">
          <div className="example-input">
            <span>
              {' '}
              类型:
              <Input placeholder="default size" />
            </span>
            <span>
              {' '}
              关键词:
              <Input placeholder="default size" />
            </span>
            <span>
              {' '}
              搜索量:
              <Input placeholder="default size" />
            </span>
          </div>
        </div>
        <div className="form">
          <Table
            rowSelection={rowSelection}
            columns={columns}
            dataSource={index.mails}
            scroll={{ x: 600, y: 400 }}
          />
        </div>
      </div>
    </div>
  );
};

export default connect((state: any) => {
  return state;
})(mail);
