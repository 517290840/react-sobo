import React, { Component, useState } from 'react';
import TablePro from '@/component/TablePro';
import { GithubIssueItem } from '@/types/index';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import {
  Button,
  Tag,
  Space,
  Input,
  Modal,
  Badge,
  Popover,
  Popconfirm,
  message,
} from 'antd';
import axios from 'axios';

import { PageDelete } from '@/services/user';
import { PageRelease } from '@/services/user';

//删除
const confirm = async (id: any) => {
  console.log(id);
  await PageDelete(id);
  message.success('Click on Yes');
  location.reload();
};
function cancel(e: any) {
  console.log(e);
  message.error('Click on No');
}
//发布/下线
const release = async (id: any) => {
  console.log(id);
  // e.stopPropagation()
  // await PageRelease(id)
};

function Page() {
  const Mycolumns: ProColumns<GithubIssueItem>[] = [
    {
      title: '名称',
      dataIndex: 'name',
      hideInSearch: false,
    },
    {
      title: '路径',
      dataIndex: 'path',
      filters: true,
      onFilter: true,
    },
    {
      title: '顺序',
      dataIndex: 'order',
      search: false,
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      search: false,
      render: (_: any, record: any) => (
        <Badge
          className="site-badge-count-109"
          count={record.views}
          showZero={true}
          key={record.id}
          style={{ backgroundColor: '#52c41a' }}
        />
      ),
    },
    {
      title: '状态',
      dataIndex: 'status',
      filters: false,

      valueEnum: {
        close: { text: '草稿', status: 'draft' },
        online: { text: '已发布', status: 'publish' },
      },
      render: (_: any, record: any) => (
        <Badge
          status={record.status === 'draft' ? 'warning' : 'processing'}
          text={record.status === 'draft' ? '草稿' : '发布成功'}
          key={record.id}
        />
      ),
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt', //要取的字段
      valueType: 'date',
      search: false, //是否显示上面的搜索框
    },
    {
      title: '操作',
      valueType: 'option',
      render: (text: any, record: any, _: any, action: any) => [
        <a
          key="editable"
          onClick={() => {
            action?.startEditable?.(record.id);
          }}
        >
          {' '}
          编辑{' '}
        </a>,

        <a href="" onClick={() => release(record.id)}>
          发布
        </a>,

        <a href="" key="exatable">
          查看访问
        </a>,

        <Popconfirm
          title="确定要删除吗?"
          onConfirm={() => confirm(record.id)}
          onCancel={cancel}
          okText="确定"
          cancelText="取消"
        >
          <a href="#">删除</a>
        </Popconfirm>,
      ],
    },
  ];
  let request: any = async (params: any = {}, sort: any, filter: any) => {
    params = {
      ...params,
      page: params.current,
      pageSize: params.pageSize,
      status:
        params.status === ('draft' || 'publish') ? params.status : undefined,
    };
    delete params.current;
    // console.log(params, "params");
    let res = await axios.get('/api/page', { params });
    // console.log(res.data.data[0],"asd");
    return {
      data: res.data.data[0],
      total: res.data.data[1],
    };
  };
  let tableAlertRender: any = ({
    selectedRowKeys,
    selectedRows,
    onCleanSelected,
  }: any) => (
    <Space size={24}>
      <Button onClick={onCleanSelected}>
        发布
        {console.log(selectedRowKeys, selectedRows)}
      </Button>
      <Button onClick={onCleanSelected}>下线</Button>
      <Button style={{ borderColor: '#f00', color: '#f00' }}>删除</Button>
    </Space>
  );
  const [according, setAccording] = useState(false); //查看访问

  const showModal = () => {
    setAccording(true);
  };
  return (
    <div>
      <TablePro
        columns={Mycolumns}
        scroll={{}}
        request={request}
        tableAlertRender={tableAlertRender}
      ></TablePro>
    </div>
  );
}

export default Page;
