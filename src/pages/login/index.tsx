import { Form, Input, Button, Checkbox } from 'antd';
import { useRequest, useHistory, useModel } from 'umi';
import { login } from '@/services/user';
import './index.less';
import axios from 'axios';
const Login = () => {
  const history = useHistory();
  console.log(history);
  const { setInitialState }: any = useModel('@@initialState');
  const { run, loading } = useRequest(login, { manual: true });
  const onFinish = async (values: any) => {
    //点击登录按钮
    let res = await run(values);
    localStorage.setItem('userInfo', JSON.stringify(res));
    setInitialState(res); //更新初始值
    history.push('/registry');
  };
  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  let getLiast = () => {
    history.push('/');
  };
  return (
    <div className="login">
      <img
        src="https://tse4-mm.cn.bing.net/th/id/OIP-C.tZQHWph4tSbHwFr9ZzBtKwAAAA?w=202&h=146&c=7&r=0&o=5&pid=1.7"
        alt=""
      />
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="用户名"
          name="name"
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="密码"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit" loading={loading}>
            登录
          </Button>
          <a
            href=""
            onClick={() => {
              history.push('/registry');
            }}
          >
            我们的跳转
          </a>
        </Form.Item>
      </Form>
    </div>
  );
};
export default Login;
