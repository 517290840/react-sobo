import React, { FC, useEffect } from 'react';
import { IndexModelState, ConnectProps, Loading, connect } from 'umi';
import { Popconfirm, message, Input } from 'antd';
import type { ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { search, SearchDelete } from '@/services/user';
interface PageProps extends ConnectProps {
  index: IndexModelState;
  loading: boolean;
}
type GithubIssueItem = {
  keyword: string;
  createAt: string;
  count: Number;
  id: string;
  subject: string;
  to: string;
};
const confirm = async (id: any) => {
  message.success('已删除');
  await SearchDelete(id);
  location.reload();
};

function cancel(e: any) {
  console.log(e);
  message.error('取消删除啦');
}
const columns: ProColumns<GithubIssueItem>[] = [
  {
    title: '类型',
    dataIndex: 'type',
    hideInTable: true,
  },
  {
    title: '搜索词',
    dataIndex: 'keyword',
  },
  {
    title: '搜索量',
    dataIndex: 'count',
  },
  {
    title: '搜索时间',
    key: 'createAt',
    dataIndex: 'createAt',
    search: false,
  },
  {
    title: '操作',
    key: 'option',

    valueType: 'option',
    render: (_, { id }, index, action) => [
      <Popconfirm
        title="Are you sure delete this task?"
        onConfirm={() => confirm(id)}
        onCancel={cancel}
        okText="Yes"
        cancelText="No"
        key="id"
      >
        <a href="#">删除</a>
      </Popconfirm>,
    ],
  },
];
const Search: FC<PageProps> = ({ index, dispatch }) => {
  console.log(index);
  useEffect(() => {
    dispatch!({
      type: 'index/getSearchList',
      payload: { page: 1, pageSize: 12 },
    });
  }, []);
  return (
    <ProTable<GithubIssueItem>
      columns={columns}
      request={async (params = {}, sort: any, filter: any) => {
        console.log(sort, filter, params);
        params = {
          ...params,
          page: params.current,
          pageSize: params.pageSize,
          status:
            params.status === ('draft' || 'publish')
              ? params.status
              : undefined,
        };
        delete params.current;
        console.log(params, 'params');
        let res = await search(params);
        console.log(res.data[0]);
        return {
          data: res.data[0],
          total: res.data[1],
        };
      }}
      rowKey="id"
      dateFormatter="string"
      rowSelection={{}}
      search={{
        defaultCollapsed: false,
        labelWidth: 'auto',
        //   optionRender: (searchConfig, formProps, dom) => [
        //     ...dom.reverse(),

        //   ],
      }}
      pagination={{
        pageSize: 5,
      }}
    />
  );
};
export default connect((state: any) => {
  return state;
})(Search);
