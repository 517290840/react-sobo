import './index.less';
import * as echarts from 'echarts';
import { useRef, useEffect } from 'react';
function index() {
  const inputEl = useRef(null);
  console.log(inputEl.current);
  useEffect(() => {
    console.log(inputEl.current);
    var myChart = echarts.init(inputEl.current);
    myChart.setOption({
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          crossStyle: {
            color: '#999',
          },
        },
      },
      toolbox: {
        feature: {
          dataView: { show: true, readOnly: false },
          magicType: { show: true, type: ['line', 'bar'] },
          restore: { show: true },
          saveAsImage: { show: true },
        },
      },
      xAxis: [
        {
          type: 'category',
          data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
          axisPointer: {
            type: 'shadow',
          },
        },
      ],
      yAxis: [
        {
          type: 'value',
          min: 0,
          max: 400,
          interval: 100,
          color: 'red',
        },
        {
          type: 'value',

          min: 0,
          max: 400,
          interval: 100,
        },
      ],
      series: [
        {
          name: '每周用户访问指标',
          type: 'bar',
          data: [120, 200, 150, 90, 80, 105, 125],
        },
        {
          name: 'Temperature',
          type: 'line',
          yAxisIndex: 1,
          data: [
            120 + 40,
            200 + 40,
            150 + 40,
            90 + 40,
            80 + 40,
            105 + 40,
            125 + 40,
          ],
        },
      ],
    });
  });
  return (
    <div className="ind">
      <div className="ind_box" ref={inputEl}></div>
    </div>
  );
}
export default index;
