import { Form, Input, Button, message, Modal } from 'antd';
import {
  SmileTwoTone,
  HeartTwoTone,
  CheckCircleTwoTone,
} from '@ant-design/icons';
import { sign } from '@/services/user';
import { useHistory, useModel } from 'umi';
import React, { useEffect, useState } from 'react';
import './index.less';
const Sign = () => {
  const [visible, setVisible] = useState(false);

  const showModal = () => {
    setVisible(true);
  };
  const handleOk = () => {
    history.push('/page/page');
    setVisible(false);
  };
  const handleCancel = () => {
    setVisible(false);
  };
  useEffect(() => {
    message.success('请注册账号');
  }, []);
  const { initialState, loading, error, refresh, setInitialState }: any =
    useModel('@@initialState');
  const history = useHistory();
  const onFinish = async (values: any) => {
    let res = await sign(values);
    res.statusCode === 201 ? setVisible(true) : '';
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div className="login">
      <div className="login_left">
        <img
          src="https://tse4-mm.cn.bing.net/th/id/OIP-C.tZQHWph4tSbHwFr9ZzBtKwAAAA?w=202&h=146&c=7&r=0&o=5&pid=1.7"
          alt=""
        />
      </div>

      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 8 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        className="login_right"
      >
        <Form.Item
          label="账号"
          name="name"
          className="oneInput"
          style={{ width: 800 }}
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="密码 "
          name="password"
          style={{ width: 800 }}
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="确认 "
          name="confirm"
          style={{ width: 800 }}
          rules={[{ required: true, message: 'Please input your confirm!' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button
            type="primary"
            htmlType="submit"
            style={{ width: 310 }}
            onClick={showModal}
          >
            注册
          </Button>
          <br />
          <div>
            Or{' '}
            <a
              href=""
              onClick={() => {
                history.push('/page/page');
              }}
            >
              去登陆!
            </a>
          </div>
        </Form.Item>
      </Form>
      <Modal visible={visible} onOk={handleOk} onCancel={handleCancel}>
        <p>
          <CheckCircleTwoTone twoToneColor="#52c41a" />
          注册成功
        </p>
        <p>是否跳转页面</p>
      </Modal>
    </div>
  );
};
export default Sign;
