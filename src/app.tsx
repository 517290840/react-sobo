import { RequestConfig } from 'umi';
import styles from './ind.less';
import { Link } from 'umi';
import { Avatar, Button } from 'antd';
import { Menu, Dropdown, message } from 'antd';
import { createLogger } from 'redux-logger';
import Topuser from '../src/components/Topuser';
import { UserOutlined, DownOutlined, GithubOutlined } from '@ant-design/icons';

export const layout = () => {
  return {
    rightContentRender: () => (
      <div>
        <Topuser />
      </div>
    ),
    onPageChange: () => {
      console.log('onpageChange');
    },
  };
};

export const request: RequestConfig = {
  timeout: 1000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [],
  responseInterceptors: [],

  headers: {
    authorization: localStorage.userInfo
      ? 'Bearer ' + JSON.parse(localStorage.userInfo).token
      : '',
  },
};

export async function getInitialState() {
  let UserInfo = localStorage.userInfo ? JSON.parse(localStorage.userInfo) : '';
  return UserInfo;
}
export const dva = {
  config: {
    onAction: createLogger(),
    onError(e: Error) {
      message.error(e.message, 3);
    },
  },
};
