import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {
  getArticle,
  filData,
  ViewList,
  searchListData,
  searchList,
  commentData,
  categoryData,
  articleData,
  mailData,
} from '@/services/user';
export interface IndexModelState {
  articleList: Array<any>;
  total: number;
  filTdata: Array<any>;

  num: number;
  ViewDta: Array<any>;
  searchData: Array<any>;
  nums: number;
  searchList: Array<any>;
  Sreactnum: number;

  comment: Array<any>;
  category: Array<any>;
  article: Array<any>;
  mail: Array<any>;
  user: Array<any>;
}

export interface IndexModelType {
  namespace: 'index';
  state: IndexModelState;
  effects: {
    query: Effect;
    fintData: Effect;
    fintnums: Effect;
    sreatchData: Effect;
    sreatchLists: Effect;
    commentent: Effect;
    categorys: Effect;
    articles: Effect;
    mails: Effect;
    users: Effect;
  };
  reducers: {
    save: Reducer;
    filTdata: Reducer;
    filTnu: Reducer;
    sreatchList: Reducer;
    sreatchListData: Reducer;
    commentDataList: Reducer;
    categoryDataList: Reducer;
    articleDataList: Reducer;
    mailDataList: Reducer;
    userDataList: Reducer;
  };
}

const IndexModel: IndexModelType = {
  namespace: 'index',
  state: {
    articleList: [],
    filTdata: [],
    total: 0,
    num: 0,
    searchData: [],
    ViewDta: [],
    nums: 0,
    searchList: [],
    Sreactnum: 0,
    comment: [],
    category: [],
    article: [],
    mail: [],
    user: [],
  },
  effects: {
    *query({ payload }, { call, put }) {
      let { data } = yield call(getArticle, payload);
      console.log(data, 'resplay');
      yield put({
        type: 'save',
        payload: data,
      });
    },
    *fintData({ payload }, { call, put }) {
      let { data } = yield call(filData, payload);
      console.log(data, 'resplay');
      yield put({
        type: 'filTdata',
        payload: data,
      });
    },

    *fintnums({ payload }, { call, put }) {
      let { data } = yield call(ViewList, payload);
      console.log(data, 'resplay');
      yield put({
        type: 'filTnu',
        payload: data,
      });
    },
    *sreatchData({ payload }, { call, put }) {
      let { data } = yield call(searchListData, payload);
      console.log(data);
      console.log(data, 'resplay');
      yield put({
        type: 'sreatchListData',
        payload: data,
      });
    },
    *sreatchLists({ payload }, { call, put }) {
      let { data } = yield call(searchList, payload);
      console.log(data);
      console.log(data, 'resplay');
      yield put({
        type: 'sreatchList',
        payload: data,
      });
    },
    *commentent({ payload }, { call, put }) {
      let { data } = yield call(commentData, payload);
      console.log(data, 'resplay');
      yield put({
        type: 'commentDataList',
        payload: data,
      });
    },
    *categorys({ payload }, { call, put }) {
      let { data } = yield call(categoryData, payload);
      console.log(data, 'resplay');
      yield put({
        type: 'categoryDataList',
        payload: data,
      });
    },
    *articles({ payload }, { call, put }) {
      let { data } = yield call(articleData, payload);
      console.log(data, 'resplay');
      yield put({
        type: 'articleDataList',
        payload: data,
      });
    },
    *mails({ payload }, { call, put }) {
      let { data } = yield call(mailData, payload);
      console.log(data, 'resplay');
      yield put({
        type: 'mailDataList',
        payload: data,
      });
    },
    *users({ payload }, { call, put }) {
      let { data } = yield call(userData, payload);
      console.log(data, 'resplay');
      yield put({
        type: 'userDataList',
        payload: data,
      });
    },
  },
  reducers: {
    save(state, { payload }) {
      return {
        articleList: payload[0],
        total: payload[1],
      };
    },
    filTdata(state, { payload }) {
      console.log(payload, 'sadfhj');
      return {
        filTdata: payload[0],
        total: payload[1],
      };
    },
    commentDataList(state, { payload }) {
      console.log(payload, 'sadfhj');
      return {
        ViewDta: payload[1],
        comment: payload[0],
      };
    },
    sreatchListData(state, { payload }) {
      console.log(payload, 'sadfhj');
      return {
        searchList: payload[1],
      };
    },
    sreatchList(state, { payload }) {
      console.log(payload, 'sadfhj');
      return {
        searchData: payload[0],
        comment: payload[0],
        total: payload[1],
      };
    },
    categoryDataList(state, { payload }) {
      console.log(payload, 'sadfhj');
      return {
        comment: payload[0],
        total: payload[1],
      };
    },
    articleDataList(state, { payload }) {
      console.log(payload, 'sadfhj');
      return {
        comment: payload[0],
        total: payload[1],
      };
    },
    filTnu(state, { payload }) {
      console.log(payload, 'sadfhj');
      return {
        ViewDta: payload[0],
        total: payload[1],
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
    mailDataList(state, { payload }) {
      console.log(payload, 'sadfhj');
      return {
        mails: payload[0],
        total: payload[1],
      };
    },
    userDataList(state, { payload }) {
      console.log(payload, 'sadfhj');
      return {
        users: payload[0],
        total: payload[1],
      };
    },
  },
};

export default IndexModel;
