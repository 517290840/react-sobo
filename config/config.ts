import { defineConfig } from 'umi';
import routes from './routes';
export default defineConfig({
  routes: routes,
  mfsu: {},
  layout: {
    name: '八维管理平台',
    logo: 'https://5b0988e595225.cdn.sohucs.com/a_auto,c_cut,x_0,y_3,w_687,h_687/images/20200508/005378c056974b3e85d2ef19b218b4fd.png',
  },
  antd: {
    // dark: true,
  },
  proxy: {
    '/api': {
      target: 'https://creationapi.shbwyz.com/',
      changeOrigin: true,
    },
  },
  dva: {
    immer: true,
    hmr: false,
  },
});
