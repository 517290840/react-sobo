export default [
  {
    path: '/newly',
    component: '@/pages/newly',
    name: '新建',
    icon: 'User',
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: false,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
  {
    path: '/',
    component: '@/pages/index',
    name: '工作台',
    icon: 'dashboard',
  },
  {
    path: '/article',
    name: '文章管理',
    icon: 'form',
    routes: [
      {
        path: '/article/allarticle',
        name: '所有文章',
        component: '@/pages/article/allarticle',
        icon: 'form',
      },
      {
        path: '/article/typearticle',
        name: '分类管理',
        component: '@/pages/article/typearticle',
        icon: 'copy',
      },
      {
        path: '/article/tagsarticle',
        name: '标签管理',
        component: '@/pages/article/tagsarticle',
        icon: 'tag',
      },
    ],
  },
  {
    path: '/page',
    component: '@/pages/page',
    name: '页面管理',
    icon: 'copy',
  },
  {
    path: '/knowledge',
    component: '@/pages/knowledge',
    name: '知识小册',
    icon: 'book',
  },
  {
    path: '/poster',
    component: '@/pages/poster',
    name: '海报管理',
    icon: 'star',
  },
  {
    path: '/comment',
    component: '@/pages/comment',
    name: '评论管理',
    icon: 'message',
  },
  {
    path: '/mail',
    component: '@/pages/mail',
    name: '邮件管理',
    icon: 'mail',
  },
  {
    path: '/file',
    component: '@/pages/file',
    name: '文件管理',
    icon: 'folderOpen',
  },
  {
    path: '/search',
    component: '@/pages/search',
    name: '搜索记录',
    icon: 'search',
  },

  {
    path: '/view',
    component: '@/pages/view',
    name: '访问统计',
    icon: 'User',
  },
  {
    path: '/user',
    component: '@/pages/user',
    name: '用户管理',
    access: 'canLogin',
    icon: 'project',
  },
  {
    path: '/setting',
    component: '@/pages/setting',
    name: '系统设置',
    icon: 'setting',
  },
  {
    path: '/login',
    component: '@/pages/login',
    name: '登录',
    // 新页面打开
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
  {
    path: '/registry',
    component: '@/pages/registry',
    name: '注册',
    // 新页面打开
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
  {
    path: '/editor',
    name: '页面新建',
    component: '@/pages/editor',
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
];
